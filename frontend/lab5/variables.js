/*variables.js*/

//esto es un comentario de linea

/*
Esto es un comentario multilinea
soporta bastantes líneas
*/

/**
*Esto es un comentario del tipo documentación
*Existen valores del tipo @param parametro
@return el tipo de valor que vamos a devolver
*/

var Uno, Dos;

Uno=1;
Dos="Dos";
console.log("El número de la variable Uno es: "+Uno);
console.log("El string de la variable Dos es: "+Dos);

//alert(Uno+Dos);
//MALA PRÄCTICA: Hay que declarar la variable
one="One variable";
console.log("La variable one tiene el siguiente contenido: "+one);
console.log(typeof(Uno));
console.log(typeof(Dos));
console.log(typeof(One));

//~
//"variables.js" 23L, 457C written